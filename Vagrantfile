# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "trusty64"
  config.vm.box_url = "https://atlas.hashicorp.com/ubuntu/boxes/trusty64/versions/14.04/providers/virtualbox.box"
  config.vm.network "public_network", bridge: "en0: Wi-Fi (Airport)"
  config.vm.network "private_network", ip: "10.0.1.92",
    virtualbox__intnet: "mynetwork"
  config.vm.provider :virtualbox do |vb|
    vb.name = "ubuntu_rails"
    # Display the VirtualBox GUI when booting the machine
    vb.gui = true
  end

  config.vm.provision "ansible" do |ansible|
      ansible.verbose = "v"
#      ansible.playbook = "playbook.yml"
       ansible.playbook = "ruby_on_rails_setup.yml"
#       ansible.inventory_path = "inventory.ini"
    end

  # Install xfce and virtualbox additions
  config.vm.provision "shell", inline: "sudo apt-get update"
  config.vm.provision "shell", inline: "sudo apt-get install -y xfce4 virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11"
  config.vm.provision "shell", inline: "sudo apt-get install build-essential"

  # Permit anyone to start the GUI
  config.vm.provision "shell", inline: "sudo sed -i 's/allowed_users=.*$/allowed_users=anybody/' /etc/X11/Xwrapper.config"
  config.vm.provision "shell", inline: "sudo apt-get install xinit"
  config.vm.provision "shell", inline: "sudo apt-get install lubuntu-desktop -y"
  config.vm.provision :shell, path: "install-rvm.sh", args: "stable", privileged: false
  config.vm.provision :shell, path: "install-ruby.sh", args: "1.9.3", privileged: false
  config.vm.provision :shell, path: "install-ruby.sh", args: "2.3.0", privileged: false
  config.vm.provision "shell", inline: <<-SHELL
    #Install Virtual Box guest additions from rpmfusion repos
    cd /vagrant
    yum install -y rpmfusion-free-release-20.noarch.rpm
    yum install -y rpmfusion-nonfree-release-20.noarch.rpm
    yum update -y
    yum install -y VirtualBox-guest

    #Add XFCE desktop to fedora server
    yum groups mark install 'graphical_environment'
    yum groupinstall -y "Xfce"
    yum install -y xorg-x11-drivers

    wget -O ruby-install-0.6.0.tar.gz \
      https://github.com/postmodern/ruby-install/archive/v0.6.0.tar.gz
    tar -xzvf ruby-install-0.6.0.tar.gz
    cd ruby-install-0.6.0/
    sudo make install
    ruby-install --latest ruby 2.3.0

  cat >> ~/.$(basename $SHELL)rc <<EOF
    source /usr/local/share/chruby/chruby.sh
    source /usr/local/share/chruby/auto.sh
  EOF
  exec $SHELL
  echo "ruby-2.3.0" >> .ruby-version
  gem install rails -v 4.2.5 --no-rdoc --no-ri
  sudo apt-get install -y libmysqlclient-dev
SHELL



  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  # config.vm.network "forwarded_port", guest: 80, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  # config.vm.provision "shell", inline: <<-SHELL
  #   apt-get update
  #   apt-get install -y apache2
  # SHELL
end
