# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
  This is the Hello World test intended to run a trivial code through the
  full life cycle.  The initial version has been committed.
* Version
  1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

## What to do next?
  1.  edit the Hello World page and change the look and feel, then commit
    and push back to the repository.
  2.  I will then add jQuery to the page, commit, and push.
  3.  We will then figure out how to use ansible to deploy to AWS.
    I will need the account information on AWS for testing.
  4.  Later revision will include database access and again run the cycle.

### Who do I talk to? ###

* Repo owner or admin
John Wooten, PhD., 865-300-4774
* Other community or team contact
